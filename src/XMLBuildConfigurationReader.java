
import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XMLBuildConfigurationReader implements IConfigReaderType {
	
	private Element docElement;

	
	public XMLBuildConfigurationReader(String fileName) {
		DocumentBuilderFactory f = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder docBuilder = f.newDocumentBuilder();
			Document doc = docBuilder.parse(new File(fileName));
			docElement = doc.getDocumentElement();
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}
	}

	private String getAttributeValue(Element elt, String attributeName) {
		return elt.getAttributes().getNamedItem(attributeName).getNodeValue();
	}
	
	public Project getProject() {
		Project project = new Project();
		project.setName(getAttributeValue(docElement,"name"));
		project.setBasedir(getAttributeValue(docElement,"basedir"));
		return project;
	}
}
