
public class ConfigurationReader {	
	
	public static void main(String[] args) {
		ConfigReader buildConfigurationReader = new ConfigReaderType(new XMLBuildConfigurationReader("build.xml"));
		System.out.println("XML Build config: " + buildConfigurationReader.getProject());
		ConfigReader yamlConfigReader = new ConfigReaderType(new YamlBuildConfigurationReader("build.xml"));
		System.out.println("XML Build config: " + yamlConfigReader.getProject());
	}
	
}