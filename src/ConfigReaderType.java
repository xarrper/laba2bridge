
public class ConfigReaderType extends ConfigReader {

	public ConfigReaderType(IConfigReaderType readerType) {
		super(readerType);
	}
	
	public Project getProject() {
		return readerType.getProject();
    }
	
}
