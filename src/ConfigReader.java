
public abstract class ConfigReader {

	IConfigReaderType readerType;
   
	public ConfigReader(IConfigReaderType readerType) {
        this.readerType = readerType;
    }
    
	public Project getProject() {
		return readerType.getProject();
    }
	
}
